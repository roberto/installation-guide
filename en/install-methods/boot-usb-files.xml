<!-- retain these comments for translator revision tracking -->
<!-- $Id$ -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Preparing Files for USB Memory Stick Booting</title>

<para>

To prepare the USB stick, we recommend to use a system where GNU/Linux is
already running and where USB is supported. With current GNU/Linux systems
the USB stick should be automatically recognized when you insert it. If
it is not you should check that the usb-storage kernel module is loaded.
When the USB stick is inserted, it will be mapped to a device named
<filename>/dev/sdX</filename>, where the <quote>X</quote> is a letter
in the range a-z. You should be able to see to which device the USB
stick was mapped by running the command <command>lsblk</command> before
and after inserting it. (The output of <command>dmesg</command> (as root) is
another possible method for that.)
To write to your stick, you may have to turn off its write
protection switch.

</para><warning><para>

The procedures described in this section will destroy anything already
on the device! Make very sure that you use the correct device name for
your USB stick. If you use the wrong device the result could be that all
information on, for example, a hard disk is lost.

</para></warning>

  <sect2 id="usb-copy-isohybrid" condition="isohybrid-supported">
  <title>Preparing a USB stick using a hybrid CD/DVD image</title>
<para>

Debian installation images for this architecture are created using the
<command>isohybrid</command> technology;
that means they can be written directly to a USB stick,
which is a very easy way to make an installation media. Simply choose
an image (such as the netinst or DVD-1) that will fit
on your USB stick. See
<xref linkend="official-cdrom"/> to get an installation image.

</para><para>

The installation image you choose should be written directly to the USB stick,
overwriting its current contents. For example, when using an existing
GNU/Linux system, the image file can be written to a USB stick
as follows, after having made sure that the stick is unmounted:

<informalexample><screen>
<prompt>#</prompt> <userinput>cp <replaceable>debian.iso</replaceable> /dev/<replaceable>sdX</replaceable></userinput>
<prompt>#</prompt> <userinput>sync</userinput>
</screen></informalexample>

Simply writing the installation image to USB like this should work fine
for most users. For special needs there is this
<ulink url="https://wiki.debian.org/DebianInstaller/CreateUSBMedia">wiki page</ulink>.

</para><para>

Information about how to do this on other operating systems can be found in
the <ulink url="&url-debian-cd-faq-write-usb;">Debian CD FAQ</ulink>.

</para><important><para>

The image must be written to the whole-disk device and not a
partition, e.g. /dev/sdb and not /dev/sdb1.
Do not use tools like <command>unetbootin</command> which alter the image.

</para></important>

  </sect2>

 </sect1>
